package is413.anthony.gameoflife;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.widget.ImageView;

/**
 * Created by Anthony on 4/14/2015.
 * <p/>
 * Utility class to check for neighbours and edge cases.
 *
 */
public class Cell
{

    private int left_bound, right_bound, top_bound, bottom_bound;

    // Get current generation and store in cells for convenience
    private int[][] cells = BoardActivity.game_box;

    public int[][] generation;

    private int neighbors = 0;

    public Cell(int left, int right, int top, int bottom)
    {
        this.left_bound = left;
        this.right_bound = right;
        this.top_bound = top;
        this.bottom_bound = bottom;

        // Create a grid to store the next generation
        generation = new int[bottom_bound][right_bound];

    }

    public void checkNeighbors(int i, int j)
    {
        // Check that the cell is within boundaries first.
        neighbors = 0;

        if (cells.length == 0)
        {return;}

        if (i < right_bound)
        {
            //check right
            if (cells[i + 1][j] == 1)
            {
                neighbors++;

            }
        }
        if (i > left_bound)
        {

            // check left
            if (cells[i - 1][j] == 1)
            {
                neighbors++;
            }

        }

        if(j < bottom_bound)
        {
            //check below
            if(cells[i][j+1] == 1)
            {
                neighbors++;
            }

        }
        if(j > top_bound)
        {
            //check above
            if(cells[i][j-1] == 1)
            {
                neighbors++;
            }
        }

        if((i > left_bound) && (j > top_bound))
        {
            // check up and left
            if(cells[i-1][j-1] == 1)
            {neighbors++;}

        }

        if((i < right_bound) && (j > top_bound))
        {
            // check up and right
            if(cells[i+1][j-1] == 1)
            {
                neighbors++;
            }
        }

        if((i > left_bound) && (j < bottom_bound))
        {
            // check down and left
            if(cells[i-1][j+1] == 1)
            {neighbors++;}

        }
        if((i < right_bound) && j < (bottom_bound))
        {
            // check down and right
            if(cells[i+1][j+1] == 1)
            {neighbors++;}

        }

        if(neighbors == 3)
        {
            generation[i][j] = 1;
        }
        if(neighbors < 2 || neighbors >= 4)
        {
            generation[i][j] = 0;
        }
        else
        {}

    }
}

