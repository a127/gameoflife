package is413.anthony.gameoflife;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Anthony on 4/16/2015.
 *
 * This class handles the drawing of the 'grid' which
 * will simulate the Game of Life.
 *
 */
public class GameBoard extends View
{
    private Paint background = new Paint();
    private Paint stroke = new Paint();
    private Paint cell = new Paint();

    private int cell_size = 50;
    boolean firstRun = true;

    public GameBoard(Context context)
    {
        // Code never called, left in for example constructor

        super(context);
        stroke.setColor(Color.RED);
        stroke.setStrokeWidth(cell_size);
        stroke.setStyle(Paint.Style.FILL);

    }

    public GameBoard(Context c, AttributeSet attributeSet)
    {
        super(c, attributeSet);

    }

    @Override
    protected void onDraw(Canvas canvas)
    {

        // Sets default colours for background and pen.

        if(firstRun) {
            background.setColor(Color.BLACK);
            background.setStyle(Paint.Style.STROKE);

            stroke.setColor(Color.RED);
            stroke.setStyle(Paint.Style.FILL);

            firstRun = false;
        }

        // Make the actual bounds outside of the drawable area to avoid
        // boundary problems.

        int height = getMeasuredHeight() / cell_size;
        int width = getMeasuredWidth() / cell_size;

        // Draw the grid using stroke rectangles for alive
        // and background rectangles for dead.

        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                if (BoardActivity.game_box[j][i] != 0)
                {
                    canvas.drawRect(
                            j * cell_size,
                            i * cell_size,
                            (j * cell_size) + cell_size,
                            (i * cell_size) + cell_size, stroke);
                }
                else
                {

                    canvas.drawRect(
                            j * cell_size,
                            i * cell_size,
                            (j * cell_size) + cell_size,
                            (i * cell_size) + cell_size, cell);

                }
            }
        }
    }

    protected void changeCellColor(String c)
    {
        c = c.toUpperCase();
        int colorVal = Color.parseColor(c);
        stroke.setColor(colorVal);

    }

    protected void changeBackgroundColor(String c)
    {
        c = c.toUpperCase();
        int colorVal = Color.parseColor(c);
        cell.setColor(colorVal);

    }
}
