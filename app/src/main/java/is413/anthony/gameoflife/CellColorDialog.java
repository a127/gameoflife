package is413.anthony.gameoflife;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Created by Xzavier Williams on 5/8/2015.
 */
public class CellColorDialog extends DialogFragment {



    //Make an array of colors
    String colors[] = {"Red", "Blue", "Green", "Yellow", "Purple", "Black"};

    public Dialog onCreateDialog(Bundle savedInstanceState){
        //Use the builder class for complete dialog constructions
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_setcellcolor)
               .setItems(colors, new DialogInterface.OnClickListener(){
                   public void onClick(DialogInterface dialog, int which){
                       //Switch statement that changes the color of the cells based on the selection

                       BoardActivity.game_board.changeCellColor(colors[which]);
                       BoardActivity.game_board.invalidate();
                   }
               });
        return builder.create();
    }

}


