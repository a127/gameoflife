package is413.anthony.gameoflife;

import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;


public class BoardActivity extends ActionBarActivity
{
    static GameBoard game_board;
    private TextView generation_count;

    int count = 0;
    static int drawMode = 0;
    static int speed = 100;

    // Get custom height and width;
    private static final int HEIGHT = 1900;
    private static final int WIDTH = 1300;
    private GameEngineTask engine;
    private boolean engine_flag = false;
    private Menu menu;

    int cell_size = 50;

    protected static int[][] game_box = new int[WIDTH][HEIGHT];

    private Cell cell = new Cell(0, WIDTH, 0, HEIGHT);

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_board, menu);
        this.menu = menu;
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            // Clears the board and the generation.
            // Resets count
            case R.id.clear_board:
                engine_flag = false;

                for (int i = 0; i < WIDTH; i++) {
                    for (int j = 0; j < HEIGHT; j++) {
                        game_box[i][j] = 0;
                    }
                }

                for (int i = 0; i < WIDTH / cell_size; i++) {
                    for (int j = 0; j < HEIGHT / cell_size; j++) {
                        cell.generation[i][j] = 0;

                    }
                }

                count = 0;
                generation_count.setText("Generation: " + count);
                game_board.invalidate();
                break;

            // Generates a glider in the top left corner,
            // located in action bar due to ease of testing.

            case R.id.glider:

                cell.generation[1][2] = 1;
                cell.generation[2][3] = 1;
                cell.generation[3][3] = 1;
                cell.generation[3][2] = 1;
                cell.generation[3][1] = 1;
                generate();
                game_board.invalidate();
                break;

            case R.id.pause_play:
                // Button works as pause and play

                engine_flag = (engine_flag == true) ? false : true;

                int icon = (engine_flag == true) ? R.drawable.ic_action_pause : R.drawable.ic_action_play;
                item.setIcon(icon);
                break;

            case R.id.change_background:
                //allows the user to change the color of the background
                BackgroundColorDialog backColorDialog = new BackgroundColorDialog();
                backColorDialog.show(getFragmentManager(), "changeBackCol");
                break;

            case R.id.change_cell:
                //allows the user to change the color of the cell
                CellColorDialog cellColorDialog = new CellColorDialog();
                cellColorDialog.show(getFragmentManager(), "changeCellCol");

                break;

            case R.id.draw_mode:
                DrawModeDialog drawModeDialog = new DrawModeDialog();
                drawModeDialog.show(getFragmentManager(), "changeDrawMo");
                break;

            case R.id.barry:
                SpeedChangeDialog speedChangeDialog = new SpeedChangeDialog();
                speedChangeDialog.show(getFragmentManager(),"ChangeSpeed");
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Matches the current gameBoard to the calculated generation.
     * Ensures that each cell is checked before redrawing the board.
     */
    private void generate()
    {
        for (int i = 0; i < WIDTH / cell_size; i++)
        {
            for (int j = 0; j < HEIGHT / cell_size; j++)
            {
                game_box[i][j] = cell.generation[i][j];

            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board);

        game_board          = (GameBoard) findViewById(R.id.game_board);
        generation_count    = (TextView) findViewById(R.id.generation_count);

        engine = new GameEngineTask();

        game_board.setOnTouchListener(new View.OnTouchListener()
        {
            /**
             *
             * Touch events stop the game_engine to allow drawing.
             * on MOTION_EVENT.ACTION_UP resumes the engine.
             *
             * @param v
             * @param event
             * @return
             */
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                int touch_x = (int) event.getX();
                int touch_y = (int) event.getY();

                engine_flag = false;

                try
                {
                    game_box[touch_x / cell_size][touch_y / cell_size] = 1;

                    v.invalidate();
                } catch (ArrayIndexOutOfBoundsException aioobe)
                {
                    return false;
                }
                if (drawMode == 0)
                {
                    if (event.getAction() == MotionEvent.ACTION_UP)
                    {
                        MenuItem g = menu.findItem(R.id.pause_play);
                        g.setIcon(R.drawable.ic_action_pause);

                        engine_flag = true;
                    }
                }

                return true;
            }
        });

        engine.execute();
    }

    private class GameEngineTask extends AsyncTask
    {

        @Override
        protected Object doInBackground(Object[] params)
        {
            while (true)
            {
                if (engine_flag)
                {
                    try
                    {
                        Thread.sleep(speed);
                    } catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                    for (int i = 0; i < WIDTH / cell_size; i++)
                    {
                        for (int j = 0; j < HEIGHT / cell_size; j++)
                        {
                            cell.checkNeighbors(i, j);

                        }
                    }
                    generate();
                    publishProgress();
                }
            }
        }
        @Override
        protected void onProgressUpdate(Object[] values)
        {
            count++;
            if(generation_count != null)
                generation_count.setText("Generation: " + count);
            game_board.invalidate();
        }
    }
}
