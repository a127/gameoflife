package is413.anthony.gameoflife;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Created by Xzavier Williams on 5/8/2015.
 *
 * Gives users five options for setting the speed of generation
 * This speed determines how long the Thread controlling the Game Engine
 * sleeps
 *
 */
public class SpeedChangeDialog extends DialogFragment {

    //Make an array of colors
    String Speeds[] = {"Slow", "Casual", "Normal", "Fast", "Maximum Overdrive"};

    public Dialog onCreateDialog(Bundle savedInstanceState){
        //Use the builder class for complete dialog constructions
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_setcellcolor)
               .setItems(Speeds, new DialogInterface.OnClickListener(){
                   public void onClick(DialogInterface dialog, int which){
                       //Switch statement that changes the color of the cells based on the selection
                       switch(Speeds[which]){
                           case "Slow":
                               BoardActivity.speed = 500;
                               break;
                           case "Casual":
                               BoardActivity.speed = 200;
                               break;
                           case "Normal":
                               BoardActivity.speed = 100;
                               break;
                           case "Fast":
                               BoardActivity.speed = 50;
                               break;
                           case "Maximum Overdrive":
                               BoardActivity.speed = 15;
                               break;
                       }

                   }
               });
        return builder.create();
    }

}


