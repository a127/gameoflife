package is413.anthony.gameoflife;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Created by Xzavier Williams on 5/8/2015.
 *
 * Switches the options for drawing on the board between
 * SIMPLE and COMPLEX.
 *
 */
public class DrawModeDialog extends DialogFragment {



    //Make an array of colors
    String modes[] = {"Simple", "Complex"};

    public Dialog onCreateDialog(Bundle savedInstanceState){
        //Use the builder class for complete dialog constructions
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Draw Mode")
               .setItems(modes, new DialogInterface.OnClickListener(){
                   public void onClick(DialogInterface dialog, int which){
                       //Switch statement that changes the color of the cells based on the selection
                       switch(modes[which]){
                           case "Simple":
                               BoardActivity.drawMode = 0;
                               break;

                           case "Complex":
                               BoardActivity.drawMode = 1;
                               break;
                       }


                   }
               });
        return builder.create();
    }

}


